package views.html

import views.html.helper.FieldConstructor
import views.html.helper.FieldElements
import views.html.metronic.metronicFieldConstructor

/**
 * Contains template helpers, for example for generating HTML forms.
 */
package object metronic {

  /**
   * Metronic input structure.
   */
  implicit val metronicField = new FieldConstructor {
    def apply(elts: FieldElements) = metronicFieldConstructor(elts)
  }

}
