package views.html.metronic

import views.html.helper.FieldConstructor
import views.html.helper.FieldElements

/**
 * Contains template helpers, for example for generating HTML forms.
 */
package object constructors {

  /**
   * Metronic input structure.
   */
  implicit val metronic9 = new FieldConstructor {
    def apply(elts: FieldElements) = metronic9FieldConstructor(elts)
  }

  implicit val vertical = new FieldConstructor {
    def apply(elts: FieldElements) = metronicVerticalConstructor(elts)
  }

}
