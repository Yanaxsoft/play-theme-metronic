name := "play-theme-metronic"

organization := "ch.insign"

version := "1.4.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaCore,
  javaJdbc,
  javaJpa,
  "org.webjars.bower" % "bootstrap" % "3.3.2",
  "org.webjars.bower" % "jquery-nestable" % "0.0.1",
  "org.webjars" % "font-awesome" % "3.2.1",
  "org.webjars" % "excanvas" % "3",
  "org.webjars" % "jquery-migrate" % "1.2.1",
  "org.webjars" % "jquery-ui" % "1.10.3",
  "org.webjars" % "select2" % "3.4.3",
  "org.webjars" % "jQuery-slimScroll" % "1.3.1",
  "org.webjars" % "jquery-blockui" % "2.65",
  "org.webjars" % "jquery-cookie" % "1.3.1",
  "org.webjars" % "uniform" % "2.1.2",
  "org.webjars" % "jquery-form" % "3.51",
  "org.webjars" % "datatables" % "1.10.11",
  "org.webjars" % "bootstrap-modal" % "2.2.5",
  "org.webjars.bower" % "bootstrap-tagsinput" % "0.8.0",
  "org.webjars.bower" % "tinymce" % "4.3.3",
  "org.webjars.bower" % "jquery-typeahead" % "2.2.1",
  "org.webjars.bower" % "chosen" % "1.4.2",
  "org.webjars.bower" % "bootstrap-switch" % "3.3.2",
  "org.webjars.bower" % "bootstrap-multiselect" % "0.9.13",
  "org.webjars.bower" % "highlightjs" % "8.5.0",
  "org.webjars.bower" % "respond" % "1.4.2",
  "org.webjars" % "jquery" % "1.10.2",
  "org.webjars.bower" % "tether" % "1.3.2"

)

routesGenerator := StaticRoutesGenerator

// Change max-filename-length for scala-Compiler. Longer filenames (not sure what the threshold is) causes problems
// with encrypted home directories under ubuntu
scalacOptions ++= Seq("-Xmax-classfile-name", "100")

// Root Path for included CSS in main.less
LessKeys.rootpath := "/"

// Adjust Urls imported in main.less
LessKeys.relativeUrls := true

// compress CSS
LessKeys.compress in Assets := true

licenses += ("Apache-2.0", url("https://www.apache.org/licenses/LICENSE-2.0.html"))
bintrayRepository := "play-cms"
bintrayOrganization := Some("insign")
publishMavenStyle := true

lazy val metronic = (project in file("."))
  .enablePlugins(PlayJava, SbtWeb)
